// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const NodeMailer = require('nodemailer');
const variables = require('./variables');
const crypto = require("crypto");

const moment = require("moment");

const functionsInTokyo = functions.region('asia-northeast1');

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
const { firebaseConfig } = require('firebase-functions');
admin.initializeApp();

const firestore = admin.firestore();





// ★★★公開用コード。テスト用ユーザのパスワードを1時間ごとに変える。不正パスワード変更に対処するため。
exports.testFunc = functionsInTokyo.pubsub.schedule('0 5 * * *')
    .timeZone('Asia/Tokyo')
    .onRun(async (context) => {
        const sampleUsers = [
            "test1@test.co.jp",
            "test2@test.co.jp",
            "test3@test.co.jp",
            "test4@test.co.jp",
            "test5@test.co.jp",
            "test6@test.co.jp",
            "test7@test.co.jp",
            "test8@test.co.jp",
            "test9@test.co.jp",
            "test10@test.co.jp",
        ]
        const users = await firestore.collection('users').where('id', 'in', sampleUsers).get();
        await Promise.all(users.docs.map(async (doc) => {
            admin
                .auth()
                .updateUser(doc.id, {
                    email: doc.data().id,
                    password: 'password',
                })
        }));
    })





exports.scheduledFunctionAt5 = functionsInTokyo.pubsub.schedule('0 5 * * *')
    .timeZone('Asia/Tokyo')
    .onRun(async (context) => {

        const today = moment(new Date().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }));
        const yesterday = moment(today).subtract(1, 'days');
        const users = await firestore.collection('users').get();

        await Promise.all(users.docs.map(async (doc) => {
            const userDoc = firestore.collection('users').doc(doc.id);
            const calendarDoc = userDoc.collection('calendar');

            await firestore.runTransaction(async transaction => {
                const todayCalendarDocs = (await transaction.get(calendarDoc.where("month", "==", today.format('yyyyMM')))).docs;
                let todayCalendar;
                if (todayCalendarDocs.length > 0) todayCalendar = todayCalendarDocs[0].data().calendar;
                else todayCalendar = {};
                const todayInfo = todayCalendar[today.format('D')];

                let yesterdayCalendar = {};
                let yesterdayCalendarDoc = null;
                let yesterdayCalendarDocs = todayCalendarDocs;
                if (yesterday.get('month') !== today.get('month')) {
                    yesterdayCalendarDocs = (await transaction.get(calendarDoc.where("month", "==", yesterday.format('yyyyMM')))).docs;
                }
                if (yesterdayCalendarDocs.length > 0) yesterdayCalendarDoc = yesterdayCalendarDocs[0];
                if (yesterdayCalendarDoc) yesterdayCalendar = yesterdayCalendarDoc.data().calendar;

                const data = doc.data();
                if (todayInfo && todayInfo.isVacation) data.status = "vacation";
                else if (data.status !== "not working") data.status = "not working";
                data.location = (todayInfo && todayInfo.location) || data.defaultLocation || "";
                const done = data.todo.filter(element => element.status === 'done').map(element => element.value);
                data.todo = data.todo.filter(element => element.status !== 'done');
                if (!(yesterday.format('D') in yesterdayCalendar)) yesterdayCalendar[yesterday.format('D')] = {};
                yesterdayCalendar[yesterday.format('D')].done = done;

                transaction.update(userDoc, { ...data });
                if (yesterdayCalendarDoc) {
                    transaction.update(calendarDoc.doc(yesterdayCalendarDoc.id), {
                        calendar: yesterdayCalendar
                    });
                }
                else {
                    transaction.set(calendarDoc.doc(), {
                        month: yesterday.format('yyyyMM'),
                        calendar: yesterdayCalendar
                    });
                }

            });
            if (today.format('D') === "1") {
                const deletedMonthDocs = (await calendarDoc.where('month', '==', moment(today).subtract(13, 'month').format('yyyyMM')).get()).docs;
                if (deletedMonthDocs.length > 0) calendarDoc.doc(deletedMonthDocs[0].id).delete();
                calendarDoc.add({
                    month: moment(today).add(11, "month").format('yyyyMM'),
                    calendar: {}
                })
            }
        }));
    })


exports.createOrganization = functionsInTokyo.firestore.document('/organizations/{organizationId}').onCreate((snapshot, context) => {
    firestore.collection('request').doc(context.params.organizationId).set({
        request: []
    });
    firestore.collection('chat').doc(context.params.organizationId).set({
        chats: []
    });

});

exports.createUserCalendar = functionsInTokyo.firestore.document('/users/{userId}').onCreate((snapshot, context) => {
    let date = moment(new Date().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' })).subtract(12, 'month');
    for (let i = 0; i < 24; i += 1) {
        firestore.collection('users').doc(context.params.userId).collection('calendar').add({
            month: date.format('yyyyMM'),
            calendar: {}
        });
        date.add(1, 'month');
    }
});

exports.updateUserAttendance = functionsInTokyo.firestore.document('/users/{userId}').onUpdate(async (change, context) => {
    const uid = context.params.userId;

    const beforeData = change.before.data();
    const afterData = change.after.data();

    if (!((beforeData.status === "not working" || beforeData.status === "vacation") && afterData.status === "working") &&
        !((beforeData.status !== "not working" && beforeData.status !== "vacation") && (afterData.status === "not working" || afterData.status === "vacation"))) return;

    let time = admin.firestore.Timestamp.now();
    await firestore.collection('users').doc(uid).update({
        attendanceTime: time
    });
    let tmpTime = time;
    if (afterData.status === "not working" || afterData.status === "vacation") tmpTime = beforeData.attendanceTime;
    const date = new Date(tmpTime.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }));

    const userDoc = firestore.collection('users').doc(uid);
    const docId = (await firestore.collection('users').doc(uid).collection('calendar').where("month", "==", moment(date).format('yyyyMM')).get()).docs[0].id;
    const calendarDoc = userDoc.collection('calendar').doc(docId);

    await firestore.runTransaction(async transaction => {
        const location = (await transaction.get(userDoc)).data().location;
        const calendar = (await transaction.get(calendarDoc)).data().calendar;
        const changeDate = moment(date).format('D');
        if (!(changeDate in calendar)) calendar[changeDate] = {};
        if (afterData.status === "not working" || afterData.status === "vacation") {
            calendar[changeDate].leaveTime = time;
            calendar[changeDate].location = location;
        }
        else if (afterData.status === "working") calendar[changeDate].attendanceTime = time;
        transaction.update(calendarDoc, { calendar: calendar });
    })

})

exports.deleteUser = functionsInTokyo.firestore.document('/users/{userId}').onDelete(async (snapshot, context) => {
    const uid = context.params.userId;
    admin.auth().deleteUser(uid);

    const data = snapshot.data()
    const requestDoc = firestore.collection("request").doc(data.organization);

    await firestore.runTransaction(async transaction => {
        const request = (await transaction.get(requestDoc)).data().request;
        const deletedRequest = request.filter(element => element.user !== data.id);
        transaction.update(requestDoc, { request: deletedRequest });
    })
})

exports.shaveChats500 = functionsInTokyo.firestore.document('/chat/{chatId}').onUpdate((change, context) => {
    const newChat = change.after.data().chats;
    const newChatLength = newChat.length;

    if (newChatLength > 500) {
        const shavedChat = newChat.slice(newChatLength - 500);
        const chatId = context.params.chatId;
        firestore.collection('chat').doc(chatId).update({
            chats: shavedChat
        });
    }
})

exports.createJoinRequest = functionsInTokyo.https.onCall(async (data, context) => {
    const randomString = createRandomString(32);

    await firestore.collection('join-request').doc(randomString).set({
        user: data.user,
        data: {
            fullname: data.fullname,
            password: encrypt(data.password)
        },
        organization: data.organization
    });

    const mailData = {
        from: 'the team run システムメール',
        to: data.user,
        subject: 'ユーザ登録確認メール',
        text: '下記URLにアクセスし、ユーザ登録を完了してください。\n(有効期限は本メールが到着してから1時間です。)\n' + variables.httpsRequestUrl + '?key=' + randomString + "\n\n\n※本メールにご心当たりのない方は、メールを破棄してください。",
    }

    await sendMail(variables.smtpData, mailData);
    setTimeout(() => {
        firestore.collection('join-request').doc(randomString).delete();
    }, 360000);

    return;

});

exports.proceedRequestAddition = functionsInTokyo.https.onRequest(async (req, res) => {
    if (req.method !== 'GET') {
        res.status(405).send('Method Not Allowed');
        return;
    }

    let joinRequest;

    try {
        joinRequest = (await firestore.collection('join-request').doc(req.query.key).get()).data();
    } catch {
        res.status(412).send('Request key is wrong');
        return;
    }

    const time = admin.firestore.Timestamp.now();

    try {
        const sameUserLength = (await firestore.collection('users').where("id", "==", joinRequest.user).get()).docs.length;
        if (sameUserLength !== 0) {
            res.status(502).send("<script>alert('Failure...\\nユーザ登録申請が失敗しました。\\n(Error: 登録済みメールアドレス)')</script>");
            return;
        }
    } catch {
        res.status(502).send("<script>alert('Failure...\\nユーザ登録申請が失敗しました。\\n(Error: メールアドレスチェック失敗)')</script>");
        return;
    }

    let docId;
    try {
        docId = (await firestore.collection('organizations').where("code", "==", joinRequest.organization).get()).docs[0].id;
    } catch {
        res.status(502).send("<script>alert('Failure...\\nユーザ登録申請が失敗しました。\\n(Error: 組織取得失敗)')</script>");
        return;
    }

    const existRequest = (await firestore.collection('request').doc(docId).get()).data().request;
    for (let request of existRequest) {
        if (request.request === "join" && request.user === joinRequest.user) {
            res.status(502).send("<script>alert('Failure...\\nユーザ登録申請が失敗しました。\\n(Error: ユーザ登録申請重複)')</script>");
            return;
        }
    }

    try {
        await firestore.collection('request').doc(docId).update({
            request: admin.firestore.FieldValue.arrayUnion({
                data: joinRequest.data,
                date: time,
                key: joinRequest.user + time,
                request: "join",
                user: joinRequest.user,
                todo: []
            })
        })
    } catch (err) {
        res.status(502).send("<script>alert('Failure...\\nユーザ登録申請が失敗しました。\\n(Error: DB追加失敗)')</script>");
        return;
    }
    firestore.collection('join-request').doc(req.query.key).delete();
    res.status(200).send("<script>alert('Success!!\\n指定された組織へユーザ登録申請を行いました。\\n組織から承認され次第、メールが送信され、ログイン可能となります。\\n承認されるまで、少々お待ちください。')</script>");
});

exports.acceptJoinUserRequest = functionsInTokyo.https.onCall(async (data, context) => {
    const user = (await firestore.collection("users").doc(context.auth.uid).get()).data();

    if (!user.isAdmin) throw new functions.https.HttpsError("not admin user operation");

    const requestDoc = firestore.collection("request").doc(user.organization);

    const request = (await requestDoc.get()).data().request;

    const joinRequest = request.find(element => element.key === data.key);

    const compoundedPassword = compound(joinRequest.data.password);

    const newUser = await admin.auth().createUser({ email: joinRequest.user, password: compoundedPassword });

    const userDoc = firestore.collection('users').doc(newUser.uid);

    await firestore.runTransaction(async transaction => {
        const request = (await transaction.get(requestDoc)).data().request;
        transaction.set(userDoc, {
            attendanceTime: admin.firestore.Timestamp.now(),
            comment: "",
            defaultLocation: "",
            fullname: joinRequest.data.fullname,
            id: newUser.email,
            isAdmin: false,
            location: "",
            organization: user.organization,
            status: "not working"
        })
        const deletedRequest = request.filter(element => element.key !== data.key);
        transaction.update(requestDoc, { request: deletedRequest });
    }).catch((err) => {
        admin.auth().deleteUser(newUser.uid);
        throw err;
    });

    const mailData = {
        from: 'the team run システムメール',
        to: joinRequest.user,
        subject: '[accepted]ユーザ登録承認メール',
        text: '組織へのユーザ登録申請が承認されました。\nthe team runへログイン可能となります。',
    }

    await sendMail(variables.smtpData, mailData);

});

exports.acceptAttendanceRequest = functionsInTokyo.https.onCall(async (data, context) => {
    const user = (await firestore.collection("users").doc(context.auth.uid).get()).data();

    if (!user.isAdmin) throw new functions.https.HttpsError("No admin user operation was rejected");

    let acceptedRequest;

    await firestore.runTransaction(async transaction => {
        const requestDoc = firestore.collection("request").doc(user.organization);
        const request = (await transaction.get(requestDoc)).data().request;
        acceptedRequest = request.find(element => element.key === data.key);

        const changedUserDoc = (await transaction.get(firestore.collection("users").where("id", "==", acceptedRequest.user))).docs[0];
        const changedUserId = changedUserDoc.id;
        const changedUser = changedUserDoc.data();

        if (user.organization !== changedUser.organization) throw new functions.https.HttpsError("Users operation is different");

        const calendarDocId = (await transaction.get(firestore.collection('users').doc(changedUserId).collection('calendar').where('month', '==', moment(acceptedRequest.data.date.toDate()).format("yyyyMM")))).docs[0].id;
        const calendarDoc = firestore.collection('users').doc(changedUserId).collection('calendar').doc(calendarDocId);

        let calendar = (await transaction.get(calendarDoc)).data().calendar;
        const date = new Date(acceptedRequest.data.date && acceptedRequest.data.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' })).getDate();
        if (!(date in calendar)) calendar[date] = {};
        calendar[date].isVacation = acceptedRequest.request === "vacation" || acceptedRequest.data.newIsVacation || false;
        calendar[date].attendanceTime = acceptedRequest.data.newAttendance;
        calendar[date].leaveTime = acceptedRequest.data.newLeave;
        transaction.update(calendarDoc, { calendar: calendar });

        const deletedRequest = request.filter(element => element.key !== data.key);
        transaction.update(requestDoc, { request: deletedRequest });
    });

    let message, subject;
    switch (acceptedRequest.request) {
        case "vacation":
            subject = "休暇取得申請承認メール";
            message = "[" + user.fullname + "]様より　" + moment(new Date(acceptedRequest.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD") + '　の休暇取得申請が承認されました。\n\n承認された休暇希望日：' + moment(new Date(acceptedRequest.data.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD");
            break;
        case "fix":
            subject = "勤務実績修正申請承認メール";
            message = "[" + user.fullname + "]様より　" + moment(new Date(acceptedRequest.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD") + '　の勤務実績修正申請が承認されました。\n\n承認された勤務実績修正日：' + moment(new Date(acceptedRequest.data.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD");
            break;
        default:
            break;
    }

    const mailData = {
        from: 'the team run システムメール',
        to: acceptedRequest.user,
        subject: '[accepted]' + subject,
        text: message,
    }

    await sendMail(variables.smtpData, mailData);
});

exports.rejectRequest = functionsInTokyo.https.onCall(async (data, context) => {
    const user = (await firestore.collection("users").doc(context.auth.uid).get()).data();

    if (!user.isAdmin) throw new functions.https.HttpsError("No admin user operation was rejected");

    const requestDoc = firestore.collection("request").doc(user.organization);

    let rejectedRequest;

    await firestore.runTransaction(async transaction => {
        const request = (await transaction.get(requestDoc)).data().request;
        const deletedRequest = request.filter(element => element.key !== data.key);
        rejectedRequest = request.find(element => element.key === data.key);
        transaction.update(requestDoc, { request: deletedRequest });
    })

    let message, subject;
    switch (rejectedRequest.request) {
        case "join":
            subject = "ユーザ登録拒否メール";
            message = '組織へのユーザ登録申請が拒否されました。\nthe team runにはログインできません。';
            break;
        case "vacation":
            subject = "休暇取得申請拒否メール";
            message = "[" + user.fullname + "]様より　" + moment(new Date(rejectedRequest.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD") + '　の休暇取得申請が拒否されました。\n\n拒否された休暇希望日：' + moment(new Date(rejectedRequest.data.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD");
            break;
        case "fix":
            subject = "勤務実績修正申請拒否メール";
            message = "[" + user.fullname + "]様より　" + moment(new Date(rejectedRequest.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD") + '　の勤務実績修正申請が拒否されました。\n\n拒否された勤務実績修正日：' + moment(new Date(rejectedRequest.data.date.toDate().toLocaleString('ja-JP', { timeZone: 'Asia/Tokyo' }))).format("yyyy/MM/DD");
            break;
        default:
            break;
    }

    const mailData = {
        from: 'the team run システムメール',
        to: rejectedRequest.user,
        subject: '[rejected]' + subject,
        text: message,
    }

    await sendMail(variables.smtpData, mailData);
});

exports.updateCalendarByUser = functionsInTokyo.https.onCall(async (data, context) => {
    const calendarDocId = (await firestore.collection('users').doc(context.auth.uid).collection('calendar').where('month', '==', data.month).get()).docs[0].id;
    const calendarDoc = firestore.collection('users').doc(context.auth.uid).collection('calendar').doc(calendarDocId);

    await firestore.runTransaction(async transaction => {
        let calendar = (await transaction.get(calendarDoc)).data().calendar;
        if (!(data.day in calendar)) calendar[data.day] = {};
        if (data.location) calendar[data.day].location = data.location;
        if (data.isVacation === false) calendar[data.day].isVacation = false;
        transaction.update(calendarDoc, { calendar: calendar });
    });
});

exports.createOrganizationWithUser = functionsInTokyo.https.onCall(async (data, context) => {
    const rootDoc = (await firestore.collection("root-users").doc(context.auth.uid).get());
    if (!rootDoc.exists) throw new functions.https.HttpsError("No root user access");

    let isExistOrganization = false;
    const organizationDocs = await firestore.collection("organizations").get();
    organizationDocs.forEach((doc) => {
        if (data.code === doc.data().code) isExistOrganization = true;
    })
    if (isExistOrganization) throw new functions.https.HttpsError("Organization code is Existed");

    const newUser = await admin.auth().createUser({ email: data.userEmail, password: data.userPassword });
    
    const newDoc = firestore.collection('organizations').doc();
    const userDoc = firestore.collection('users').doc(newUser.uid);
    
    await firestore.runTransaction(async transaction => {
        transaction.set(newDoc, {
            code: data.code,
            name: data.name,
        });
        transaction.set(userDoc, {
            attendanceTime: admin.firestore.Timestamp.now(),
            comment: "",
            defaultLocation: "",
            fullname: data.userFullname,
            id: newUser.email,
            isAdmin: true,
            location: "",
            organization: newDoc,
            status: "not working"
        })
    }).catch((err) => {
        admin.auth().deleteUser(newUser.uid);
        throw err;
    }); 
});

exports.deleteOrganization = functionsInTokyo.https.onCall(async (data, context) => {
    const rootDoc = (await firestore.collection("root-users").doc(context.auth.uid).get());
    if (!rootDoc.exists) throw new functions.https.HttpsError("No root user access");

    let organizationDoc = null;
    const organizationDocs = await firestore.collection("organizations").get();
    organizationDocs.forEach((doc) => {
        if (data.code === doc.data().code && data.name === doc.data().name) {
            organizationDoc = doc;
        }
    })
    if (organizationDoc === null) throw new functions.https.HttpsError("No such organization");

    const chatDoc = firestore.collection('chat').doc(organizationDoc.id);
    const requestDoc = firestore.collection('request').doc(organizationDoc.id);
    const userDocs = await firestore.collection('users').where('organization', '==', organizationDoc.id).get();
    
    await firestore.runTransaction(async transaction => {
        transaction.delete(organizationDoc);
        transaction.delete(chatDoc);
        transaction.delete(requestDoc);
        userDocs.forEach((doc) => {
            transaction.delete(doc);
        });
    }).catch((err) => {
        admin.auth().deleteUser(newUser.uid);
        throw err;
    }); 
});

function createRandomString(len) {
    const charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    const cl = charSet.length;
    let r = "";
    for (let i = 0; i < len; i++) {
        r += charSet[Math.floor(Math.random() * cl)];
    }

    return r;
}

async function sendMail(smtpData, mailData) {

    const transporter = NodeMailer.createTransport(smtpData)

    transporter.sendMail(mailData, function (error, info) {
        if (error) {
            throw error;
        } else {
            console.log('Email sent: ' + info.response)
        }
    })
}

// 暗号化
function encrypt(planeText) {
    var cipher = crypto.createCipher('aes192', variables.password);
    cipher.update(planeText, 'utf8', 'hex');
    return cipher.final('hex');
}

// 複合
function compound(cipheredText) {
    var decipher = crypto.createDecipher('aes192', variables.password);
    decipher.update(cipheredText, 'hex', 'utf8');
    return decipher.final('utf8');
}